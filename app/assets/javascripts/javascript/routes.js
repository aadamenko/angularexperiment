angular
.module('AngExp')
.config(function($routeProvider){

	$routeProvider
		.when("/", {
			redirectTo: '/experiments'
		})
		.when("/experiments", {
			templateUrl: 'templates/experiments/index.html',
			controller: 'IndexExpController'
		})
		.when('/experiments/new', {
			templateUrl: 'templates/experiments/new.html',
			controller: 'CreateExpController'
		})
		.when('/experiments/:id', {
			templateUrl: 'templates/experiments/show.html',
			controller: 'ShowExpController'
		})
		.when('/experiments/:id/edit', {
			templateUrl: 'templates/experiments/edit.html',
			controller: 'EditExpController'
		})
});