angular
.module('AngExp')
.factory('Experiment', function($resource) {

  return $resource('data/experimentsData.json'); // Note the full endpoint address
  
});