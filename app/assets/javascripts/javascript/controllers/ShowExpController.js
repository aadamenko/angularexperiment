angular
.module('AngExp')
.controller('ShowExpController', function(Experiment, $scope, $routeParams, $location){

	$scope.experiments = Experiment.query();


	$scope.deleteExp = function(experiment){
		experiment.$remove().then(function(){
			$location.path("/experiments");
		});
	};
});